rule fastq_countbases:
    input:
        fq = rules.fastq_trim_adapter.output.R1
    output:
        "tempfiles/{sample}.basepercentages.done"
    resources:
        mem_mb=3000        
    run:
        if sample_in_db(wildcards.sample, serie, METRICSDB, 'basepercentages'):
            shell("touch {output}")
        elif not sample_in_db(wildcards.sample, serie, METRICSDB, 'basepercentages'):
            counts = get_basecounts(input.fq)
            base_perc_reads2db(wildcards.sample, serie, counts, METRICSDB)
            shell("touch {output}")


rule bam_metrics_capture:
    input:
        bam=rules.bam_markduplicates.output.bam
    output:
        temp("tempfiles/{sample}.HSMetrics.txt")
    resources:
        mem_mb=3000        
    log:
        "logfiles/{sample}.HSmetrics.log"
    run:
        target = input_dict[wildcards.sample]['picard']
        serie = input_dict[wildcards.sample]['serie']
        if sample_in_db(wildcards.sample, serie, METRICSDB, 'hsmetrics'):
            shell("touch {output}")

        elif not sample_in_db(wildcards.sample, serie, METRICSDB, 'hsmetrics'):
            shell('''{PICARD} CalculateHsMetrics R={REF} I={input} \
            O={output[0]} TI={target} BI={target} > {log} 2>&1
            ''')
            metrics = readmetrics(wildcards.sample, serie, output[0])
            metrics2db(metrics, METRICSDB, 'hsmetrics')


rule bam_metrics_insertsize:
    input:
        bam=rules.bam_markduplicates.output.bam
    output:
        tabel = temp("tempfiles/{sample}.InsertSize.txt"),
        pdf = temp("tempfiles/{sample}.InsertSize.pdf")
    resources:
        mem_mb=3000        
    log:
        "logfiles/{sample}.InsertSize.log"
    run:
        serie = input_dict[wildcards.sample]['serie']
        if sample_in_db(wildcards.sample, serie, METRICSDB, 'insertsize'):
            shell("touch {output.pdf}")
            shell("touch {output.tabel}")
        elif not sample_in_db(wildcards.sample, serie, METRICSDB, 'insertsize'):
            shell('''{PICARD} CollectInsertSizeMetrics R={REF} I={input} \
            O={output.tabel} HISTOGRAM_FILE={output.pdf} > {log} 2>&1
            ''')
            metrics = readmetrics(wildcards.sample, serie, output.tabel)
            metrics2db(metrics, METRICSDB, 'insertsize')


rule bam_metrics_alignment:
    input:
        bam=rules.bam_markduplicates.output.bam
    output:
        temp("tempfiles/{sample}.AlignmentMetrics.txt")
    resources:
        mem_mb=3000        
    log:
        "logfiles/{sample}.AlignmentMetrics.log"
    run:
        serie = input_dict[wildcards.sample]['serie']

        if sample_in_db(wildcards.sample, serie, METRICSDB, 'alignment'):
            shell("touch {output}")
        elif not sample_in_db(wildcards.sample, serie, METRICSDB, 'alignment'):
            shell('''{PICARD} CollectAlignmentSummaryMetrics R={REF} I={input} \
            O={output[0]}  MAX_INSERT_SIZE=500 > {log} 2>&1
            ''')
            serie = input_dict[wildcards.sample]['serie']
            metrics = readmetrics(wildcards.sample, serie, output[0])
            metrics2db(metrics, METRICSDB, 'alignment')


rule bam_callableloci:
    input:
        bam = rules.bam_markduplicates.output.bam,
        table = rules.recalibrate.output
    output:
        bed = temp("tempfiles/{sample}.bed"),
        summary = temp("tempfiles/{sample}.summary")
    resources:
        mem_mb=3000
    log:
        "logfiles/{sample}.CallableLoci.log"
    run:
        if sample_in_db(wildcards.sample, serie, METRICSDB, 'sangers'):
            shell('touch {output.summary} {output.bed}')
        else:
            target = input_dict[wildcards.sample]['sanger']
            shell('''{JAVA} -Xmx3g -jar {GATK} -R {REF} -T CallableLoci \
            -minDepth 30 -mdflmq 30 -mmq 20 -frlmq 0.6 -mlmq 10 \
            -I {input.bam} -o {output.bed} \
            -summary {output.summary}  \
            -L {target} --BQSR {input.table} > {log} 2>&1''')
            if input_dict[wildcards.sample]['panel'] is None:
                db_targetname = input_dict[wildcards.sample]['pakket']
            else:
                db_targetname = input_dict[wildcards.sample]['panel']
            summary2db(wildcards.sample, read_summary(output.summary),
                       db_targetname, serie, METRICSDB)


rule bam_depthofcoverage:
    input:
        bamfile = rules.bam_markduplicates.output.bam,
        table = rules.recalibrate.output
    output:
        "tempfiles/{sample}.DoC",
        temp("tempfiles/{sample}.DoC.sample_cumulative_coverage_counts"),
        temp("tempfiles/{sample}.DoC.sample_cumulative_coverage_proportions"),
        temp("tempfiles/{sample}.DoC.sample_interval_statistics"),
        temp("tempfiles/{sample}.DoC.sample_statistics"),
        temp("tempfiles/{sample}.DoC.sample_summary"),
        "tempfiles/{sample}.DoC.sample_interval_summary"
    resources:
        mem_mb=3000
    log:
        "logfiles/{sample}.DepthOfCoverage.log"        
    run:
        target = input_dict[wildcards.sample]['cnvtarget']
        shell('''{JAVA} -Xmx3g -jar {GATK} -R {REF} -T DepthOfCoverage \
        -I {input.bamfile} -o {output[0]} \
        -L {target} \
        --minBaseQuality 20 \
        --minMappingQuality 20 \
        --printBaseCounts  \
        -dels -BQSR {input.table} > {log} 2>&1
        ''')

        if input_dict[wildcards.sample]['capispakket']:
            perc_covered = calc_perc_target_covered(output[0])
            pakket = input_dict[wildcards.sample]['capture']

        elif not input_dict[wildcards.sample]['capispakket']:
            pakket_target = input_dict[wildcards.sample]['pakkettarget']
            perc_covered = calc_perc_target_covered(output[0],
                                                    filter_regions=True,
                                                    target=pakket_target)
            pakket = input_dict[wildcards.sample]['pakket']

        mean, std = mean_doc(output[0])
        target = input_dict[wildcards.sample]['capture']
        mean_std_2db(mean, std, serie, wildcards.sample, target, METRICSDB)    
        perc_target_covered2db(wildcards.sample, perc_covered, pakket, serie, METRICSDB)
