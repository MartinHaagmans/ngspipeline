rule fastq_trim_adapter:
    input:
        {SAMPLESHEET}
    output:
        R1=temp("tempfiles/{sample}.trimmed.R1.fastq.gz"),
        R2=temp("tempfiles/{sample}.trimmed.R2.fastq.gz")
    resources:
        mem_mb=4000        
    log:
        "logfiles/{sample}.fastp.log"
    run:
        R1 = glob.glob(f'{wildcards.sample}.R1.fastq.gz')
        R2 = glob.glob(f'{wildcards.sample}.R2.fastq.gz')    
        shell('{FASTP} -i {R1} -I {R2} -o {output.R1} -O {output.R2} --verbose > {log}  2>&1')


rule fastq_delete_input:
    input:
        rules.fastq_trim_adapter.output
    output:
        "tempfiles/{sample}.fastq.removed"
    priority:
        10
    run:
        R1 = glob.glob(f'{wildcards.sample}.R1.fastq.gz')
        R2 = glob.glob(f'{wildcards.sample}.R2.fastq.gz')      
        shell('rm -f {R1} {R2} && touch {output}')


rule fastq_align_reads:
    input:
        rules.fastq_trim_adapter.output.R1,
        rules.fastq_trim_adapter.output.R2
    output:
        temp("tempfiles/{sample}.sorted.bam")
    resources:
        mem_mb=6000
    log:
        "logfiles/{sample}.BWAlignment.log"
    params:
        rg = "@RG\\tID:{sample}\\tLB:{sample}\\tPL:ILLUMINA\\tPU:{sample}\\tSM:{sample}"
    shell:
        '''(bwa mem  -R '{params.rg}' -t 1 -M {REF} {input} |\
        samtools view -Shu - |\
        samtools sort -T {wildcards.sample}.tmp -O bam - > {output}) > {log}  2>&1
        '''


rule bam_markduplicates:
    input:
        rules.fastq_align_reads.output
    output:
        bam = "output/{sample}.DM.bam",
        metrics = temp("tempfiles/{sample}.dupmark.txt")
    resources:
        mem_mb=3000
    log:
        "logfiles/{sample}.MarkDuplicates.log"
    shell:
        '''{PICARD} MarkDuplicates  I={input} O={output.bam}  \
        M={output.metrics} REMOVE_DUPLICATES=FALSE CREATE_INDEX=true > {log}  2>&1
        '''
