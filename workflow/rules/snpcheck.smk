rule bam_callsnpcheck:
    input:
        rules.bam_markduplicates.output.bam
    output:
        vcf = "tempfiles/{sample}.snpcheck.vcf",
        index = "tempfiles/{sample}.snpcheck.vcf.idx"
    resources:
        mem_mb=4000        
    log:
        "logfiles/{sample}.CallSNPcheck.log"
    run:
        if sample_in_db(wildcards.sample, serie, METRICSDB, 'snpcheck'):
            shell('touch {output.vcf} {output.index}')  
        elif not sample_in_db(wildcards.sample, serie, METRICSDB, 'snpcheck'):
            shell('''{JAVA} -Xmx4g -jar {GATK} -R {REF} -T HaplotypeCaller \
                -I {input} -o {output.vcf} -L {SNPCHECK} -alleles {SNPCHECK} \
                -gt_mode GENOTYPE_GIVEN_ALLELES -GQB 0 > {log} 2>&1
                ''')


rule vcf_compare_snpcheck:
    input:
        vcf = rules.bam_callsnpcheck.output.vcf,
        alt = "SNPcheck/{sample}.qpcrsnpcheck"
    output:
        temp("SNPcheck/{sample}.snpcheck.done.txt")
    resources:
        mem_mb=500        
    log:
        "logfiles/{sample}.CompareSNPcheck.log"
    run:
        if sample_in_db(wildcards.sample, serie, METRICSDB, 'snpcheck'):
            shell('touch {output}')
        else:
            store_in_db = dict()
            store_in_db['NGS'] = parse_ngssnpcheck(input.vcf)
            store_in_db['ALT'] = parse_taqman(input.alt)
            store_in_db['COMP'] = compare_snpchecks(store_in_db['ALT'], store_in_db['NGS'])
            store_in_db_json = json.dumps(store_in_db)
            snpcheck2db(wildcards.sample, serie, store_in_db_json, METRICSDB)
            shell('touch {output}')

                