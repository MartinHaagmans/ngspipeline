rule bam_call_variants:
    input:
        bamfile = rules.bam_markduplicates.output.bam,
        table = rules.recalibrate.output
    output:
        temp("tempfiles/{sample}.raw.vcf"),
        temp("tempfiles/{sample}.raw.vcf.idx")
    resources:
        mem_mb=4000        
    log:
        "logfiles/{sample}.HaplotypeCaller.log"
    run:
        target = input_dict[wildcards.sample]['varcal']
        shell('''{JAVA} -Xmx4g -jar {GATK} -R {REF} -T HaplotypeCaller \
        -I {input.bamfile} -o {output[0]} -L {target} -ip 500 \
        -pairHMM VECTOR_LOGLESS_CACHING -BQSR {input.table}  > {log} 2>&1
        ''')


rule vcf_filter_variants:
    input:
        vcf = rules.bam_call_variants.output[0],
        index = rules.bam_call_variants.output[1]
    output:
        "output/{sample}.filtered.vcf"
    resources:
        mem_mb=2000
    log:
        "logfiles/{sample}.VariantFiltration.log"
    shell:
        '''{JAVA} -Xmx2g -jar {GATK} -R {REF} -T VariantFiltration \
        -V {input.vcf} -o {output} \
        --clusterWindowSize 20 --clusterSize 6 \
        --filterExpression "DP < 30 " --filterName "LowCoverage" \
        --filterExpression "QUAL < 50.0 " --filterName "LowQual" \
        --filterExpression "QD < 4.0 " --filterName "QD" \
        --filterExpression "SOR > 10.0 " --filterName "SOR" > {log} 2>&1
        '''


rule sample_add_cnv_data:
    input:
        rules.bam_depthofcoverage.output[-1]
    output:
        temp("tempfiles/{sample}.cnv_data_added.txt")
    resources:
        mem_mb=500        
    log:
        "logfiles/{sample}.CNV_add_data.log"
    run:
        if input_dict[wildcards.sample]['cnvscreening']:
            target = input_dict[wildcards.sample]['capture']
            shell("CNV -c {target} -s {serie} -n {input} --sample {wildcards.sample} --addonly > {log} 2>&1")
            shell("touch {output}")
        else:
            shell("touch {output}")


rule serie_call_cnv:
    input:
        expand(rules.sample_add_cnv_data.output, sample=samples)
    output:
        "tempfiles/cnvdetection.txt"
    resources:
        mem_mb=500        
    log:
        "logfiles/CNV_detector.log"
    run:
        for capture in captures:
            screening = False
            for s in samples:
                workdir = os.path.join(os.getcwd(), 'output', 'CNV_{}'.format(capture))
                if input_dict[s]['capture'] == capture:
                    screening = input_dict[s]['cnvscreening']
            if screening:
                os.makedirs(workdir, exist_ok=True)
                shell("CNV -c {capture} -s {serie} -o {workdir} > {log} 2>&1")
        shell("touch {output}")
