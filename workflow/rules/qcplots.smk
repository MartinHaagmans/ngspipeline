__author__ = "Martin Haagmans (https://gitlab.com/MartinHaagmans)"
__license__ = "MIT"

rule serie_qc_plots:
    input:
        expand(rules.fastq_countbases.output, sample=samples),
        expand(rules.bam_metrics_insertsize.output, sample=samples),
        expand(rules.vcf_filter_variants.output, sample=samples)
    output:
        "output/NSX{}_QC.pdf".format(serie)
    resources:
        mem_mb=500
    run:
        import vcf
        import pybedtools
        import seaborn as sns
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_pdf import PdfPages
        from ngsscriptlibrary import get_df_baseperc_reads_serie

        def get_insert_size_serie(serie):
            conn = sqlite3.connect(METRICSDB)
            sql = '''
            SELECT DISTINCT SAMPLE, MEAN_INSERT_SIZE,
            STANDARD_DEVIATION FROM insertsize WHERE SERIE='{}'
            '''.format(serie)
            df = pd.read_sql(sql, con=conn, index_col='SAMPLE')
            df.columns = 'Mean', 'StDev'
            df.index.rename('Sample', inplace=True)
            df.sort_index(inplace=True)
            conn.close()
            return df

        def plot_basepercentages(df, outputpdf):
            df.index = [i.split('.')[0] for i in df.index]
            df.sort_index(inplace=True)
            sample_target = list()
            for sample in sorted(input_dict.keys()):
                if not input_dict[sample]['amplicon']:
                    target = input_dict[sample]['capture']
                    sample_target.append((sample, target))
            index = pd.MultiIndex.from_tuples(sample_target,
                                              names=['Target', 'index'])
            df.index = index
            df.index.rename(['Sample ID', 'Capture'], inplace=True)
            df.sort_index(inplace=True)
            for pakket, data in df.groupby('Capture'):
                fig = plt.figure(figsize=(12, 9))
                ax = plt.subplot()
                data.plot(ax=ax, style='8', clip_on=False, rot=90,
                        use_index=True, ylim=(0, 0.5),
                        title='Library QC: Basepercentages R1')
                xlabel = ['{}:{}'.format(i[0], i[1]) for i in data.index]
                xticks = np.arange(0, len(xlabel), 1.0)
                ax.xaxis.set_ticks(xticks)
                ax.set_xticklabels(xlabel)
                plt.tight_layout()
                outputpdf.savefig()
                plt.close()

        def plot_insertsizes(df, outputpdf):
            sample_target = list()
            for sample in df.index:
                target = input_dict[sample]['capture']
                sample_target.append((sample, target))
            index = pd.MultiIndex.from_tuples(sample_target,
                                              names=['Target', 'index'])
            df.index = index
            df.index.rename(['Sample ID', 'Capture'], inplace=True)
            df.sort_index(inplace=True)
            for pakket, data in df.groupby('Capture'):
                fig = plt.figure(figsize=(12, 9))
                ax = plt.subplot()
                data.plot(ax=ax, kind='bar', rot=90, use_index=True,
                    ylim=(0, 350), title='Library QC: InsertSize')
                xlabel = ['{}:{}'.format(i[0], i[1]) for i in data.index]
                xticks = np.arange(0, len(xlabel), 1.0)
                ax.xaxis.set_ticks(xticks)
                ax.set_xticklabels(xlabel)
                plt.tight_layout()
                outputpdf.savefig()
                plt.close()            

        def plot_varpercentages_vcf(vcffile, plotfile, targetfile, title):
            vcfreader = vcf.Reader(open(vcffile, 'r'))
            try:
                varpercentages = [[(call.data.AD[1] / record.INFO['DP'])
                                  for call in record]
                                  for record in vcfreader
                                  if record.is_snp and not record.FILTER]
            except AttributeError:
                varpercentages = list()
                
            fig = plt.figure()
            ax = plt.subplot()
            ax.plot(varpercentages, 'd')
            ax.set_ylim(0, 1)
            ax.axhline(y=0.5)
            plt.title(title)
            try:
                plt.savefig(plotfile, dpi=80)
            except (TypeError, ValueError, AttributeError):
                plotfile.savefig()
            plt.close()

        pdf = PdfPages(output[0])
        plot_basepercentages(get_df_baseperc_reads_serie(serie, METRICSDB), pdf)
        plot_insertsizes(get_insert_size_serie(serie), pdf)
        for sample in samples:
            pakkettarget = input_dict[sample]['pakkettarget']
            t = pybedtools.BedTool(pakkettarget)
            v = pybedtools.BedTool('output/{}.filtered.vcf'.format(sample))
            x = v.intersect(t, header=True)
            plot_varpercentages_vcf(x.fn, pdf, pakkettarget,
                                    '{}:{}'.format(sample, input_dict[sample]['pakket']))
        pdf.close()
