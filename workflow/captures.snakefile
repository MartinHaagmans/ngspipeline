"""
AMC pipeline to process NovaSeq data for diagnostics.
Martin Haagmans https://gitlab.com/MartinHaagmans
January 2024
"""
import os
import csv
import sys
import glob
import json
import shutil
import sqlite3
import subprocess

import vcf
import matplotlib
import xlsxwriter
import numpy as np
import pandas as pd

from ngsscriptlibrary.parsing import *

from ngsscriptlibrary import metrics2db
from ngsscriptlibrary import summary2db
from ngsscriptlibrary import sangers2db
from ngsscriptlibrary import snpcheck2db
from ngsscriptlibrary import mean_std_2db
from ngsscriptlibrary import sample_in_db
from ngsscriptlibrary import get_patient_info
from ngsscriptlibrary import get_rs_gpos_dict
from ngsscriptlibrary import compare_snpchecks
from ngsscriptlibrary import get_file_locations
from ngsscriptlibrary import samplesheetinfo2db
from ngsscriptlibrary import base_perc_reads2db
from ngsscriptlibrary import get_snpcheck_serie
from ngsscriptlibrary import perc_target_covered2db
from ngsscriptlibrary import riskscore_and_genotypes_2db
from ngsscriptlibrary import parse_sangers_for_seriereport

matplotlib.use('Agg')

# Define variables from config file
GATK = config["GATK"]
REF = config["REF"]
FASTP = config["FASTP"]
DBSNP = config["DBSNP"]
PICARD = config["PICARD"]
TARGETREPO = config["TARGET"]
DBREPO = config['DB']
JAVA = config['JAVA']

# Define files
TARGETDB = os.path.join(TARGETREPO, 'varia', 'captures.sqlite')
SNPCHECK = os.path.join(TARGETREPO, 'varia', 'NGS-SNPcheck.vcf')
METRICSDB = os.path.join(DBREPO, 'metrics.sqlite')
SAMPLEDB = os.path.join(DBREPO, 'samplesheets.sqlite')
PATIENTDB = os.path.join(DBREPO, 'patientinfo.sqlite')

SAMPLESHEET = 'SampleSheet.csv'
STANDAARDFRAGMENTEN = 'standaardfragmenten.xlsx'

for fn in [TARGETDB, SNPCHECK, METRICSDB, SAMPLEDB, PATIENTDB, SAMPLESHEET]:
    assert os.path.isfile(fn), '{} bestaat niet'.format(fn)

def add_patient_info(todo, serie, db):
    for sample in todo.keys():
        try:
            sex, ff, dob, deadline = get_patient_info(sample, serie, db)
        except Exception as e:
            sex, ff, dob, deadline = [0, 0, 0, 0]
            print(e)
        if sex == 'M':
            sex = 'Man'
        elif sex == 'V':
            sex = 'Vrouw'
        todo[sample]['geslacht'] = sex
        todo[sample]['FFnummer'] = ff
        todo[sample]['geboortedatum'] = dob
        todo[sample]['deadline'] = deadline
    return todo

def create_recalinput_file(samples):
    """Create a file with a list of BAM-files (1 per sample) to use as input
    for the recalibration step in the pipeline.
    """
    with open('recalinput.list', 'w') as f:
        for sample in samples:
            f.write('output/{}.DM.bam\n'.format(sample))
            
# Get todo list for samples
input_dict = parse_samplesheet_for_pipeline(SAMPLESHEET, TARGETDB, ['Amplicon', 'Amplicons'])
input_dict = get_file_locations(input_dict, TARGETREPO)

captures = get_captures(input_dict)
pakketten = get_pakketten(input_dict)
samples = [s for s in input_dict.keys() if not input_dict[s]['amplicon']]
serie = [input_dict[_]['serie'] for _ in input_dict.keys()][0]
input_dict = add_patient_info(input_dict, serie, PATIENTDB)

# Rules for snakemake

include:
    'rules/alignment.smk'

  
rule all:
    input:
        expand("output/{sample}.filtered.vcf", sample=samples),
        expand("output/{sample}.xlsx", sample=samples),
        expand("output/{sample}.DM.bam", sample=samples),
        expand("tempfiles/{sample}.fastq.removed", sample=samples),
        "output/input.json",
        "output/NSX{}_report.xlsx".format(serie),
        "output/NSX{}_QC.pdf".format(serie)


rule dump_input_dict:
    input:
        {SAMPLESHEET}
    output:
        "output/input.json"
    run:
        with open("{}".format(output), 'w') as fp:
            json.dump(input_dict, fp, indent=4)


rule recalibrate:
    input:
        expand("output/{sample}.DM.bam", sample=samples)
    output:
        "reads/bqsr.grp"
    resources:
        mem_mb=10000        
    log:
        "logfiles/Recal.log"
    threads:
        10
    run:
        create_recalinput_file(samples)
        shell('''{JAVA} -Xmx10g -jar {GATK} -R {REF} -T BaseRecalibrator -knownSites {DBSNP} \
        -I recalinput.list -o {output} -nct {threads} > {log} 2>&1
        ''')


include:
    'rules/metrics.smk'


rule getsangers:
    input:
        rules.bam_callableloci.output.bed
    output:
        sangers = "tempfiles/{sample}.sangers.txt",
        tempbed = temp("tempfiles/{sample}.tmp.bed")
    resources:
        mem_mb=500        
    run:
        annot_bed = input_dict[wildcards.sample]['annot']
        panel = input_dict[wildcards.sample]['panel']
        if panel is None:
            target_name = input_dict[wildcards.sample]['pakket']
        elif panel is not None:
            target_name = panel
        sangers_for_db = list()
        if sample_in_db(wildcards.sample, serie, METRICSDB, 'sangers'):
            pass
        elif panel is not None and 'OVR' in panel:
            sangers_for_db = 'Geen sangers: OVR panel'
            sangers2db(sangers_for_db, serie, wildcards.sample, target_name, METRICSDB)
        else:
            df = annotate_callables(input, annot_bed, output.tempbed)
            sangercount = 0
            for target, data in df.groupby(df.index):
                if len(data[data['callable'] != 'CALLABLE']) > 0:
                    sangercount += 1
                    gene = ' '.join(data['gene'].unique())
                    start = data[data['callable'] != 'CALLABLE']['regionstart'].min()
                    end = data[data['callable'] != 'CALLABLE']['regionend'].max()
                    sangers_for_db.append((gene, target[0], str(start), str(end),
                                           str(target[1]), str(target[2])))
            if sangercount == 0:
                sangers_for_db = 'Geen sangers: alles callable'
            sangers2db(sangers_for_db, serie,  wildcards.sample, target_name, METRICSDB)
        shell("touch {output.sangers} {output.tempbed}")


include:
    'rules/snpcheck.smk'
include:
    'rules/variants.smk'    
include:
    'rules/callmosaic.smk'
include:
    'rules/samplereport.smk'
include:
    'rules/seriereport.smk'
include:
    'rules/qcplots.smk'